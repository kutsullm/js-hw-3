let number = prompt("Enter a number"); 

if (number) { 
  let count = 0;
  for (let i = 0; i <= number; i++) {
    if (i % 5 === 0) { 
      console.log(i);
      count++;
    }
  }
  if (count === 0) {
    console.log("Sorry, no numbers");
  }
} else {
  console.log("Sorry, please enter a number");
}